package com.ascrud.authmore.platform;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.oauth.JSONWebTokenManager;
import com.ascrud.authmore.oauth.OAuthProperties;
import com.ascrud.authmore.oauth.TokenResponse;
import com.ascrud.authmore.repositories.ClientDetailsRepository;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyPair;
import java.text.ParseException;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Bootstrap.class})
public class BootstrapTests {

    @Autowired
    private KeyPair keyPair;

    @Autowired
    private ClientDetailsRepository clients;

    @Autowired
    private JWKSet jwkSet;

    @Test
    public void testJSONWebTokenManager() throws ParseException, JOSEException, BadJOSEException {

        JSONWebTokenManager tokens = new JSONWebTokenManager(clients, keyPair);
        ClientDetails client = clients.findAll().get(0);
        String userId = "user_1";
        TokenResponse tokenResponse = tokens.create(client, userId, Collections.emptySet());
        String accessToken;
        assertNotNull(tokenResponse);
        assertNotNull(accessToken = tokenResponse.getAccess_token());
        ConfigurableJWTProcessor<SecurityContext> jwtProcessor = new DefaultJWTProcessor<>();
        JWKSource<SecurityContext> keySource = new ImmutableJWKSet<>(jwkSet);
        JWSAlgorithm expectedJWSAlg = JWSAlgorithm.RS256;
        JWSKeySelector<SecurityContext> keySelector = new JWSVerificationKeySelector<>(expectedJWSAlg, keySource);
        jwtProcessor.setJWSKeySelector(keySelector);
        JWTClaimsSet claimsSet = jwtProcessor.process(accessToken, null);
        assertEquals(userId, claimsSet.getClaim(OAuthProperties.TOKEN_USER_ID));
    }
}

