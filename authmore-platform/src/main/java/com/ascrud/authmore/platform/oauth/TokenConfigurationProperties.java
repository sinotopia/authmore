package com.ascrud.authmore.platform.oauth;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author walkman
 * @since 2019-05-30
 */
@Component
@ConfigurationProperties(prefix = "token")
public class TokenConfigurationProperties {

    private final static String ENV_TOKEN_TYPE = "token_type";

    private String tokenType = System.getenv(ENV_TOKEN_TYPE);

    enum TokenPolicy {
        REDIS, JWT
    }

    private TokenPolicy policy;

    public TokenPolicy getPolicy() {
        String tokenTypeName = getTokenTypeName();
        if (isEmpty(tokenTypeName)) {
            return policy;
        }
        switch (tokenTypeName) {
            case "jwt":
                return TokenPolicy.JWT;
            case "redis":
                return TokenPolicy.REDIS;
            default:
                return policy;
        }
    }

    private String getTokenTypeName() {
        String name = null;
        if (!isEmpty(tokenType)) {
            name = tokenType.toLowerCase();
        }
        return name;
    }

    public void setPolicy(TokenPolicy policy) {
        this.policy = policy;
    }
}
