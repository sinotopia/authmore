package com.ascrud.authmore.platform.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.UserDetails;
import com.ascrud.authmore.oauth.*;
import com.ascrud.authmore.oauth.*;
import com.ascrud.authmore.platform.authorization.UserDetailsRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * @author walkman
 * @since 2019-03-03
 */
@Component
public final class TokenPasswordTokenIssuer {

    private final UserDetailsRepository users;
    private final PasswordEncoder passwordEncoder;
    private final TokenManager tokenManager;

    public TokenPasswordTokenIssuer(
            UserDetailsRepository users,
            PasswordEncoder passwordEncoder,
            TokenManager tokenManager) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
        this.tokenManager = tokenManager;
    }

    public TokenResponse issue(ClientDetails client, String username, String password, String scope) {
        OAuthUtil.validateClientAndGrantType(client, OAuthProperties.GrantTypes.PASSWORD);
        UserDetails user = users.findByUsername(username)
                .orElseThrow(() -> new OAuthException("invalid username"));
        boolean matches = passwordEncoder.matches(password, user.getPassword());
        if (!matches)
            throw new OAuthException("invalid password");
        OAuthUtil.validateClientAndScope(client, scope);
        String userId = user.getId();
        Set<String> scopes = OAuthUtil.scopeSet(scope);
        return tokenManager.create(client, userId, scopes);
    }
}
