package com.ascrud.authmore.platform.authorization;

import com.ascrud.authmore.UserDetails;
import com.ascrud.authmore.authorization.AuthenticationException;
import com.ascrud.authmore.authorization.AuthenticationManager;
import com.ascrud.authmore.authorization.SessionManager;
import com.ascrud.authmore.authorization.SessionProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;

import javax.servlet.http.HttpSession;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author walkman
 * @since 2019-02-15
 */
@Controller
public class AuthenticationEndpoint {

    private static final String ERROR = "error";

    private final AuthenticationManager authenticationManager;
    private final SessionManager sessionManager;

    public AuthenticationEndpoint(AuthenticationManager authenticationManager, SessionManager sessionManager) {
        this.authenticationManager = authenticationManager;
        this.sessionManager = sessionManager;
    }

    @GetMapping("/signin")
    public String signInPage() {
        return "signin";
    }

    @PostMapping("/signin")
    public String singIn(
            @RequestParam("ui") String userName,
            @RequestParam("uc") String inputPassword,
            @RequestParam(value = "ur",required = false) boolean rememberMe,
            @SessionAttribute(SessionProperties.LAST_URL) String from,
            HttpSession session,
            Model model) {
        UserDetails user;
        try {
            user = authenticationManager.userValidate(userName, inputPassword);
        } catch (AuthenticationException e) {
            model.addAttribute(ERROR, e.getMessage());
            return "/signin";
        }
        sessionManager.signin(user);
        if (!rememberMe)
            session.setAttribute(SessionProperties.FORGET_ME, Boolean.TRUE);
        if (!isEmpty(from))
            return "redirect:" + from;
        return "redirect:/";
    }
}
