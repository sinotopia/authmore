package com.ascrud.authmore.platform.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.oauth.OAuthUtil;
import com.ascrud.authmore.oauth.TokenManager;
import com.ascrud.authmore.oauth.TokenResponse;
import org.springframework.stereotype.Component;

import java.util.Set;

import static com.ascrud.authmore.oauth.OAuthProperties.GrantTypes.CLIENT_CREDENTIALS;

/**
 * @author walkman
 * @since 2019-03-03
 */
@Component
public final class TokenClientCredentialsTokenIssuer {

    private final TokenManager tokenManager;

    public TokenClientCredentialsTokenIssuer(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    public TokenResponse issue(ClientDetails client, String scope) {
        OAuthUtil.validateClientAndGrantType(client, CLIENT_CREDENTIALS);
        OAuthUtil.validateClientAndScope(client, scope);
        Set<String> scopes = OAuthUtil.scopeSet(scope);
        return tokenManager.create(client, null, scopes);
    }
}
