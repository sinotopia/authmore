package com.ascrud.authmore.platform.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.authorization.RequestProperties;
import com.ascrud.authmore.oauth.OAuthFilter;
import com.ascrud.authmore.oauth.OAuthProperties;
import com.ascrud.authmore.oauth.OAuthUtil;
import com.ascrud.authmore.repositories.ClientDetailsRepository;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author walkman
 * @since 2019-02-19
 */
@WebFilter(urlPatterns = {"/oauth/token", "/oauth/check_token"})
public class TokenAuthenticationFilter extends OAuthFilter {

    private final ClientDetailsRepository clients;
    private final PasswordEncoder passwordEncoder;

    public TokenAuthenticationFilter(ClientDetailsRepository clients, PasswordEncoder passwordEncoder) {
        this.clients = clients;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String clientId;
        String clientSecret;
        clientId = request.getParameter(OAuthProperties.PARAM_CLIENT_ID);
        clientSecret = request.getParameter(OAuthProperties.PARAM_CLIENT_SECRET);
        if (isEmpty(clientId) || isEmpty(clientSecret)) {
            String authorization = request.getHeader(AUTHORIZATION);
            if (null == authorization || !authorization.startsWith("Basic")) {
                sendError(response, "basic authentication is required");
                return;
            }
            Map<String, String> client = OAuthUtil.extractClientFrom(request);
            clientId = client.get(OAuthProperties.PARAM_CLIENT_ID);
            clientSecret = client.get(OAuthProperties.PARAM_CLIENT_SECRET);
        }
        if (isEmpty(clientId) || isEmpty(clientSecret)) {
            sendError(response, "client_id or client_secret is required");
            return;
        }
        Optional<ClientDetails> find = clients.findByClientId(clientId);
        if (!find.isPresent()) {
            sendError(response, "invalid client");
            return;
        }
        ClientDetails client = find.get();
        if (!passwordEncoder.matches(clientSecret, client.getPassword())) {
            sendError(response, "invalid password");
            return;
        }
        request.setAttribute(RequestProperties.CURRENT_CLIENT, client);
        filterChain.doFilter(request, response);
    }
}
