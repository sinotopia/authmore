package com.ascrud.authmore.platform.authorization;

import com.ascrud.authmore.UserDetails;
import com.ascrud.authmore.authorization.SessionManager;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import static com.ascrud.authmore.authorization.SessionProperties.CURRENT_USER;
import static com.ascrud.authmore.authorization.SessionProperties.CURRENT_USER_DETAILS;

/**
 * @author walkman
 * @since 2019-02-15
 */
@Service
public class UserSessionManager implements SessionManager {

    private HttpSession session;

    public UserSessionManager(HttpSession session) {
        this.session = session;
    }

    @Override
    public void signin(UserDetails user) {
        session.setAttribute(CURRENT_USER_DETAILS, user);
        session.setAttribute(CURRENT_USER, user.getUsername());
    }
}
