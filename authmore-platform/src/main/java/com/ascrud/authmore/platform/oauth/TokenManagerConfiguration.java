package com.ascrud.authmore.platform.oauth;

import com.ascrud.authmore.platform.authorization.RedisCodeManager;
import com.ascrud.authmore.oauth.CodeManager;
import com.ascrud.authmore.oauth.JSONWebTokenManager;
import com.ascrud.authmore.oauth.RedisTokenManager;
import com.ascrud.authmore.oauth.TokenManager;
import com.ascrud.authmore.platform.authorization.CodeRepository;
import com.ascrud.authmore.platform.oauth.TokenConfigurationProperties.TokenPolicy;
import com.ascrud.authmore.repositories.AccessTokenRepository;
import com.ascrud.authmore.repositories.ClientDetailsRepository;
import com.ascrud.authmore.repositories.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import java.security.KeyPair;

/**
 * @author walkman
 * @since 2019-03-03
 */
@Configuration
public class TokenManagerConfiguration {

    private final TokenConfigurationProperties tokenConfiguration;
    private final KeyPair keyPair;

    @Autowired
    public TokenManagerConfiguration(TokenConfigurationProperties tokenConfiguration, KeyPair keyPair) {
        this.tokenConfiguration = tokenConfiguration;
        this.keyPair = keyPair;
    }

    @Bean
    @ConditionalOnMissingBean({TokenManager.class})
    public TokenManager tokenManager(
            AccessTokenRepository tokens,
            RefreshTokenRepository refreshTokens,
            RedisTemplate<String, String> redisTemplate, ClientDetailsRepository clients) {
        if (null != tokenConfiguration && tokenConfiguration.getPolicy() == TokenPolicy.JWT)
            return new JSONWebTokenManager(clients, keyPair);
        return new RedisTokenManager(tokens, refreshTokens, redisTemplate, clients);
    }

    @Bean
    @ConditionalOnMissingBean({CodeManager.class})
    public CodeManager codeManager(CodeRepository codes) {
        return new RedisCodeManager(codes);
    }
}
