package com.ascrud.authmore.platform.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.authorization.CodeBinding;
import com.ascrud.authmore.oauth.CodeManager;
import com.ascrud.authmore.oauth.OAuthException;
import com.ascrud.authmore.oauth.TokenManager;
import com.ascrud.authmore.oauth.TokenResponse;
import org.springframework.stereotype.Component;

import java.util.Set;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author walkman
 * @since 2019-03-02
 */
@Component
final class TokenAuthorizationCodeTokenIssuer {

    private final CodeManager codeManager;
    private final TokenManager tokenManager;

    public TokenAuthorizationCodeTokenIssuer(CodeManager codeManager, TokenManager tokenManager) {
        this.codeManager = codeManager;
        this.tokenManager = tokenManager;
    }

    public TokenResponse issue(ClientDetails client, String redirectUri, String code) {
        CodeBinding codeBinding = codeManager.getCodeDetails(client.getClientId(), code);
        Set<String> scopes = codeBinding.getScopes();
        String requestRedirectUri = codeBinding.getRedirectUri();
        if (isEmpty(redirectUri) || !redirectUri.equals(requestRedirectUri)) {
            throw new OAuthException(OAuthException.REDIRECT_URI_MISMATCH);
        }
        codeManager.expireCode(code);
        String userId = codeBinding.getUserId();
        return tokenManager.create(client, userId, scopes);
    }
}
