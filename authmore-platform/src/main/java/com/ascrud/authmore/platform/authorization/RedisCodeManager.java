package com.ascrud.authmore.platform.authorization;

import com.ascrud.authmore.authorization.AbstractCodeManager;
import com.ascrud.authmore.authorization.CodeBinding;
import com.ascrud.authmore.oauth.OAuthException;

import static com.ascrud.authmore.oauth.OAuthException.INVALID_CODE;

/**
 * @author walkman
 * @since 2019-02-18
 */
public class RedisCodeManager extends AbstractCodeManager {

    private final CodeRepository authorizationCodes;

    public RedisCodeManager(CodeRepository authorizationCodes) {
        super();
        this.authorizationCodes = authorizationCodes;
    }

    @Override
    public void saveCode(CodeBinding codeBinding) {
        authorizationCodes.save(codeBinding);
    }

    @Override
    public CodeBinding getCodeDetails(String clientId, String code) {
        return authorizationCodes.findById(code)
                .orElseThrow(() -> new OAuthException(INVALID_CODE));
    }

    @Override
    public void expireCode(String code) {
        authorizationCodes.deleteById(code);
    }
}
