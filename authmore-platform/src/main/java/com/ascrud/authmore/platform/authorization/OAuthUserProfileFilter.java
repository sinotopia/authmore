package com.ascrud.authmore.platform.authorization;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.UserDetails;
import com.ascrud.authmore.oauth.*;
import com.ascrud.authmore.repositories.ClientDetailsRepository;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ascrud.authmore.oauth.OAuthProperties.REQUEST_AUTHORITIES;
import static com.ascrud.authmore.oauth.OAuthProperties.REQUEST_SCOPES;

/**
 * @author walkman
 * @since 2019-02-25
 */
@WebFilter(urlPatterns = {"/user/details"})
public class OAuthUserProfileFilter extends OAuthFilter {

    private final TokenManager tokens;
    private final ClientDetailsRepository clients;
    private final UserDetailsRepository users;

    public OAuthUserProfileFilter(TokenManager tokens, ClientDetailsRepository clients, UserDetailsRepository users) {
        this.tokens = tokens;
        this.clients = clients;
        this.users = users;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        AccessTokenBinding accessTokenBinding;
        Set<String> authorities;
        Set<String> scopes;
        String token;
        try {
            token = OAuthUtil.extractToken(request);
        } catch (OAuthException e) {
            sendError(response, e.getMessage());
            return;
        }
        try {
            accessTokenBinding = tokens.findAccessToken(token);
        } catch (OAuthException e) {
            sendError(response, e.getMessage());
            return;
        }
        String clientId = accessTokenBinding.getClientId();
        ClientDetails client = clients.findByClientId(clientId)
                .orElseThrow(() -> new OAuthException(OAuthException.INVALID_CLIENT));
        scopes = accessTokenBinding.getScopes();
        String userId = accessTokenBinding.getUserId();
        if (null != userId) {
            UserDetails user = users.findById(userId).orElseThrow(() -> new OAuthException("no such user"));
            authorities = user.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toSet());
        } else {
            authorities = client.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
        }
        request.setAttribute(REQUEST_SCOPES, scopes);
        request.setAttribute(REQUEST_AUTHORITIES, authorities);
        filterChain.doFilter(request, response);
    }
}
