package com.ascrud.authmore.platform;

import com.ascrud.authmore.ErrorResponse;
import com.ascrud.authmore.oauth.OAuthErrorResponse;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @author walkman
 * @since 2019-03-05
 */
@RestController
public class ErrorEndpoint extends AbstractErrorController {

    public ErrorEndpoint(ErrorAttributes errorAttributes) {
        super(errorAttributes);
    }

    @RequestMapping("/error")
    public ErrorResponse error(HttpServletRequest request) {
        Map<String, Object> errorAttributes = super.getErrorAttributes(request, false);
        String message = (String) errorAttributes.getOrDefault("message", "unknown error");
        return new OAuthErrorResponse(message, "no description");
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
