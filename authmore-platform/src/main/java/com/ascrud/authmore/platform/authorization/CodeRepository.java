package com.ascrud.authmore.platform.authorization;

import com.ascrud.authmore.authorization.CodeBinding;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author walkman
 * @since 2019-02-19
 */
@Repository
public interface CodeRepository extends CrudRepository<CodeBinding, String> {}
