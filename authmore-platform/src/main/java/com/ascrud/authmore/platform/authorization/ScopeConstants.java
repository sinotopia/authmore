package com.ascrud.authmore.platform.authorization;

import java.util.Arrays;

/**
 * @author walkman
 * @since 2019-05-30
 */
public enum ScopeConstants {

    AVATAR("头像"),
    EMAIL("邮件"),
    PROFILE("个人资料"),
    NICKNAME("昵称"),
    GENDER("性别"),
    AGE("年龄"),
    ADDRESS("地址"),
    UNKNOWN("其它范围");

    private String name;

    ScopeConstants(String name) {
        this.name = name;
    }

    public String findByKey() {
        return name;
    }

    public static String findByKey(String key) {
        return Arrays.stream(ScopeConstants.values()).filter(v -> v.name().equals(key))
                .findFirst()
                .orElse(ScopeConstants.UNKNOWN)
                .findByKey();
    }
}
