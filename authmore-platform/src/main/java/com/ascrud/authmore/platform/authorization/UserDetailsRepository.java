package com.ascrud.authmore.platform.authorization;

import com.ascrud.authmore.UserDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author walkman
 * @since 2019-02-14
 */
@Repository
public interface UserDetailsRepository extends MongoRepository<UserDetails, String> {

    Optional<UserDetails> findByUsername(String userName);
}
