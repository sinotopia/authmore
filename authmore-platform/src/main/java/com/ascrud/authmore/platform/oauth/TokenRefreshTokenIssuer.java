package com.ascrud.authmore.platform.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.oauth.OAuthUtil;
import com.ascrud.authmore.oauth.TokenManager;
import com.ascrud.authmore.oauth.TokenResponse;
import org.springframework.stereotype.Component;

import static com.ascrud.authmore.oauth.OAuthProperties.GrantTypes.REFRESH_TOKEN;

/**
 * @author walkman
 * @since 2019-03-03
 */
@Component
public final class TokenRefreshTokenIssuer {

    private final TokenManager tokenManager;

    public TokenRefreshTokenIssuer(TokenManager tokenManager) {
        this.tokenManager = tokenManager;
    }

    public TokenResponse issue(ClientDetails client, String refreshToken) {
        OAuthUtil.validateClientAndGrantType(client, REFRESH_TOKEN);
        return tokenManager.refresh(refreshToken);
    }
}
