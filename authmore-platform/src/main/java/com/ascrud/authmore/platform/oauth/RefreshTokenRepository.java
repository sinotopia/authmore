package com.ascrud.authmore.platform.oauth;

import org.springframework.stereotype.Repository;

/**
 * @author walkman
 * @since 2019-05-19
 */
@Repository
public interface RefreshTokenRepository extends com.ascrud.authmore.repositories.RefreshTokenRepository {}
