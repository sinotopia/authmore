package com.ascrud.authmore.resource;

import com.ascrud.authmore.oauth.OAuthProperties;

import java.lang.annotation.*;

/**
 * @author walkman
 * @since 2019-02-27
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface AuthorityRequired {

    OAuthProperties.RequireTypes type() default OAuthProperties.RequireTypes.ANY;

    String[] value() default {};
}
