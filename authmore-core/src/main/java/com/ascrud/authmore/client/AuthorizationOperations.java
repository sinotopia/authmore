package com.ascrud.authmore.client;

import com.ascrud.authmore.oauth.OAuthProperties.ResponseTypes;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author walkman
 * @since 2019-03-07
 */
public interface AuthorizationOperations {

    void redirectToUserAuthorize(HttpServletResponse response, ResponseTypes type, String scope) throws IOException;
}
