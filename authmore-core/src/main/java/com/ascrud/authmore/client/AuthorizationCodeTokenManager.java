package com.ascrud.authmore.client;

import com.ascrud.authmore.common.Assert;
import com.ascrud.authmore.oauth.OAuthProperties;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static com.ascrud.authmore.oauth.OAuthProperties.GrantTypes;

/**
 * @author walkman
 * @since 2019-03-02
 */
public final class AuthorizationCodeTokenManager extends AbstractClientTokenManager {

    private String redirectUri;

    public AuthorizationCodeTokenManager(
            RestTemplate client,
            String clientId,
            String clientSecret,
            String tokenIssueUrl,
            String redirectUri) {
        super(client, clientId, clientSecret, tokenIssueUrl);
        this.redirectUri = redirectUri;
    }

    @Override
    protected void enhanceQueryParams(Map<String, String> params) {
        super.enhanceQueryParams(params);
        params.put(OAuthProperties.PARAM_REDIRECT_URI, redirectUri);
        String code = params.get(OAuthProperties.PARAM_CODE);
        Assert.notEmpty(code, "code cannot be empty");
    }

    @Override
    protected final GrantTypes getGrantType() {
        return GrantTypes.AUTHORIZATION_CODE;
    }
}
