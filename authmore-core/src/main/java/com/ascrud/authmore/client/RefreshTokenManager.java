package com.ascrud.authmore.client;

import com.ascrud.authmore.common.Assert;
import com.ascrud.authmore.oauth.OAuthProperties;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author walkman
 * @since 2019-03-02
 */
public final class RefreshTokenManager extends AbstractClientTokenManager {

    public RefreshTokenManager(
            RestTemplate client,
            String clientId,
            String clientSecret,
            String tokenIssueUrl) {
        super(client, clientId, clientSecret, tokenIssueUrl);
    }

    @Override
    protected void enhanceQueryParams(Map<String, String> params) {
        super.enhanceQueryParams(params);
        String refreshToken = params.get(OAuthProperties.PARAM_REFRESH_TOKEN);
        Assert.notEmpty(refreshToken, refreshToken);
    }

    @Override
    protected final OAuthProperties.GrantTypes getGrantType() {
        return OAuthProperties.GrantTypes.REFRESH_TOKEN;
    }
}
