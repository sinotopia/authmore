package com.ascrud.authmore.client;

import org.springframework.web.client.RestTemplate;

import static com.ascrud.authmore.oauth.OAuthProperties.GrantTypes;

/**
 * @author walkman
 * @since 2019-03-02
 */
public final class ClientCredentialsTokenManager extends AbstractClientTokenManager {

    public ClientCredentialsTokenManager(
            RestTemplate client,
            String clientId,
            String clientSecret,
            String tokenIssueUrl) {
        super(client, clientId, clientSecret, tokenIssueUrl);
    }

    @Override
    protected final GrantTypes getGrantType() {
        return GrantTypes.CLIENT_CREDENTIALS;
    }
}
