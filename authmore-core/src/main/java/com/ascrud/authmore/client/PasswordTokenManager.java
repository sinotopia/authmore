package com.ascrud.authmore.client;

import com.ascrud.authmore.common.Assert;
import com.ascrud.authmore.oauth.OAuthProperties;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author walkman
 * @since 2019-03-02
 */
public final class PasswordTokenManager extends AbstractClientTokenManager {

    public PasswordTokenManager(
            RestTemplate client,
            String clientId,
            String clientSecret,
            String tokenIssueUrl) {
        super(client, clientId, clientSecret, tokenIssueUrl);
    }

    @Override
    protected void enhanceQueryParams(Map<String, String> params) {
        super.enhanceQueryParams(params);
        String userName = params.get(OAuthProperties.PARAM_USERNAME);
        String password = params.get(OAuthProperties.PARAM_PASSWORD);
        Assert.notEmpty(userName, "username cannot be empty");
        Assert.notEmpty(password, "password cannot be empty");
    }

    @Override
    protected final OAuthProperties.GrantTypes getGrantType() {
        return OAuthProperties.GrantTypes.PASSWORD;
    }
}
