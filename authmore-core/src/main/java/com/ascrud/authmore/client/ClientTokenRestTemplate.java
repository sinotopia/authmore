package com.ascrud.authmore.client;

import org.springframework.web.client.RestTemplate;

/**
 * @author walkman
 * @since 2019-03-02
 */
public class ClientTokenRestTemplate extends RestTemplate {}
