package com.ascrud.authmore.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author walkman
 * @since 2019-05-17
 */
@Controller
public class ImplicitAuthorizationEndpoint {

    private final ClientConfigurationProperties clientProperties;

    @Autowired
    public ImplicitAuthorizationEndpoint(ClientConfigurationProperties clientProperties) {
        this.clientProperties = clientProperties;
    }

    @GetMapping("/implicit.html")
    public String view(Model model) {
        String callBackUri = clientProperties.getImplicitTokenUri();
        model.addAttribute("callBackUri", callBackUri);
        return "implicit";
    }
}
