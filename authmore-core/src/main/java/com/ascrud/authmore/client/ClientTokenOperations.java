package com.ascrud.authmore.client;

import com.ascrud.authmore.oauth.TokenResponse;

import java.util.Map;

/**
 * @author walkman
 * @since 2019-03-02
 */
public interface ClientTokenOperations {

    TokenResponse getToken(String scope, Map<String, String> restParams);
}
