package com.ascrud.authmore.client;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author walkman
 * @since 2019-03-01
 */
@ConfigurationProperties(prefix = "authmore.client")
public final class ClientConfigurationProperties {

    private String clientId;

    private String clientSecret;

    private String tokenIssueUrl;

    private String authorizeUrl;

    private Boolean requestTokenOnStartup = false;

    private String scope;

    private String redirectUri;

    private String implicitTokenUri;

    private String implicitRedirectUri;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getTokenIssueUrl() {
        return tokenIssueUrl;
    }

    public void setTokenIssueUrl(String tokenIssueUrl) {
        this.tokenIssueUrl = tokenIssueUrl;
    }

    public boolean isRequestTokenOnStartup() {
        return requestTokenOnStartup;
    }

    public void setRequestTokenOnStartup(boolean requestTokenOnStartup) {
        this.requestTokenOnStartup = requestTokenOnStartup;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getAuthorizeUri() {
        return authorizeUrl;
    }

    public void setAuthorizeUri(String authorizeUrl) {
        this.authorizeUrl = authorizeUrl;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    public String getImplicitTokenUri() {
        return implicitTokenUri;
    }

    public void setImplicitTokenUri(String implicitTokenUri) {
        this.implicitTokenUri = implicitTokenUri;
    }

    public String getImplicitRedirectUri() {
        return implicitRedirectUri;
    }

    public void setImplicitRedirectUri(String implicitRedirectUri) {
        this.implicitRedirectUri = implicitRedirectUri;
    }
}
