package com.ascrud.authmore.client;

import org.springframework.http.client.support.BasicAuthenticationInterceptor;

/**
 * @author walkman
 * @since 2019-03-01
 */
public class ClientHttpClientInterceptor extends BasicAuthenticationInterceptor {

    public ClientHttpClientInterceptor(String username, String password) {
        super(username, password);
    }
}
