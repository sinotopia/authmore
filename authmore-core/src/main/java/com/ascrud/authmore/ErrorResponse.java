package com.ascrud.authmore;

/**
 * @author walkman
 * @since 2019-03-05
 */
public interface ErrorResponse {

    String getError();

    String getError_description();
}
