package com.ascrud.authmore.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.common.UniqueToken;
import com.ascrud.authmore.repositories.ClientDetailsRepository;

import java.util.Set;

import static com.ascrud.authmore.oauth.OAuthException.INVALID_CLIENT;
import static com.ascrud.authmore.oauth.OAuthException.INVALID_SCOPE;

/**
 * @author walkman
 * @since 2019-02-21
 */
public abstract class AbstractTokenManager implements TokenManager {

    private final ClientDetailsRepository clients;

    public AbstractTokenManager(ClientDetailsRepository clients) {
        this.clients = clients;
    }

    @Override
    public TokenResponse create(ClientDetails client, String userId, Set<String> scopes) {
        String clientId = client.getClientId();
        long expireIn = client.getAccessTokenValiditySeconds();
        assertValidateScopes(client, scopes);
        AccessTokenBinding accessTokenBinding = createAccessTokenBinding(clientId, scopes, userId);
        String refreshToken = UniqueToken.create();
        RefreshTokenBinding refreshTokenBinding = new RefreshTokenBinding(refreshToken, clientId, scopes, userId);
        saveAccessToken(accessTokenBinding);
        saveRefreshToken(refreshTokenBinding);
        expireAccessToken(accessTokenBinding.getAccessToken(), expireIn);
        long expireAt = OAuthUtil.expireAtByLiveTime(expireIn);
        accessTokenBinding.setExpire(expireAt);
        Integer refreshTokenValiditySeconds = client.getRefreshTokenValiditySeconds();
        if (null != refreshTokenValiditySeconds && 0 != refreshTokenValiditySeconds) {
            expireRefreshToken(refreshToken, refreshTokenValiditySeconds);
        }
        saveAccessToken(accessTokenBinding);
        return new TokenResponse(accessTokenBinding.getAccessToken(), expireIn, refreshToken, scopes);
    }

    @Override
    public TokenResponse refresh(String refreshToken) {
        RefreshTokenBinding refreshTokenBinding = findRefreshToken(refreshToken);
        String clientId = refreshTokenBinding.getClientId();
        ClientDetails client = clients.findByClientId(clientId).orElseThrow(() -> new OAuthException(INVALID_CLIENT));
        long expireIn = client.getAccessTokenValiditySeconds();
        String newAccessToken = UniqueToken.create();
        refreshTokenBinding = freshRefreshTokenBinding(client, refreshTokenBinding);
        TokenResponse newTokenResponse = new TokenResponse(refreshTokenBinding, newAccessToken, expireIn);
        AccessTokenBinding newAccessTokenBinding = new AccessTokenBinding(refreshTokenBinding, newAccessToken);
        saveAccessToken(newAccessTokenBinding);
        expireAccessToken(newAccessToken, expireIn);
        long expireAt = OAuthUtil.expireAtByLiveTime(expireIn);
        newAccessTokenBinding.setExpire(expireAt);
        saveAccessToken(newAccessTokenBinding);
        return newTokenResponse;
    }

    private AccessTokenBinding createAccessTokenBinding(String clientId, Set<String> scopes, String userId) {
        String accessToken = UniqueToken.create();
        return new AccessTokenBinding(accessToken, clientId, scopes, userId);
    }

    protected void assertValidateScopes(ClientDetails client, Set<String> scopes) {
        boolean validScope = client.getScope().containsAll(scopes);
        if (!validScope)
            throw new OAuthException(INVALID_SCOPE);
    }

    @Override
    public RefreshTokenBinding freshRefreshTokenBinding(ClientDetails client, RefreshTokenBinding refreshTokenBinding) {
        Integer refreshTokenValiditySeconds = client.getRefreshTokenValiditySeconds();
        if (null != refreshTokenValiditySeconds && 0 != refreshTokenValiditySeconds) {
            String newRefreshToken = UniqueToken.create();
            Set<String> scopes = refreshTokenBinding.getScopes();
            String userId = refreshTokenBinding.getUserId();
            String clientId = client.getClientId();
            refreshTokenBinding = new RefreshTokenBinding(newRefreshToken, clientId, scopes, userId);
            saveRefreshToken(refreshTokenBinding);
            expireRefreshToken(newRefreshToken, refreshTokenValiditySeconds);
        }
        return refreshTokenBinding;
    }
}
