package com.ascrud.authmore.oauth;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.authorization.CodeBinding;

import java.util.Set;

/**
 * @author walkman
 * @since 2019-02-18
 */
public interface CodeManager {

    void saveCodeBinding(ClientDetails client, String code, Set<String> scopes, String redirectUri, String userId);

    CodeBinding getCodeDetails(String clientId, String code);

    void expireCode(String code);
}
