package com.ascrud.authmore.oauth;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author walkman
 * @since 2019-03-01
 */
public abstract class OAuthFilter extends OncePerRequestFilter {

    public void sendError(HttpServletResponse response) throws IOException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "oauth unauthorized");
    }

    public void sendError(HttpServletResponse response, String message) throws IOException {
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, message);
    }

    public void sendError(HttpServletResponse response, String message, int status) throws IOException {
        response.sendError(status, message);
    }
}
