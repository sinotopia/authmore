package com.ascrud.authmore.authorization;

/**
 * @author walkman
 * @since 2019-02-15
 */
public interface SessionProperties {

    String CURRENT_USER = "current_user";
    String CURRENT_USER_DETAILS = "current_user_details";
    String LAST_URL = "last_url";
    String CURRENT_CLIENT = "current_client";
    String CURRENT_REDIRECT_URI = "current_redirect_uri";
    String LAST_STATE = "last_state";
    String LAST_SCOPE = "last_scope";
    String LAST_TYPE = "last_type";
    String FORGET_ME = "forget_me";
}
