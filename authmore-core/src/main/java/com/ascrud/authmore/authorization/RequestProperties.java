package com.ascrud.authmore.authorization;

/**
 * @author walkman
 * @since 2019-02-26
 */
public interface RequestProperties {

    String CURRENT_CLIENT = "current_client";
}
