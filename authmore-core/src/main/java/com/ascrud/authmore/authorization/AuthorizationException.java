package com.ascrud.authmore.authorization;

/**
 * @author walkman
 * @since 2019-02-18
 */
public class AuthorizationException extends RuntimeException {

    public AuthorizationException() {
        super("authorization_error");
    }

    public AuthorizationException(String message) {
        super(message);
    }

    public AuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }
}
