package com.ascrud.authmore.authorization;

import com.ascrud.authmore.UserDetails;

/**
 * @author walkman
 * @since 2019-02-15
 */
public interface SessionManager {

    void signin(UserDetails user);
}
