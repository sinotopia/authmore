package com.ascrud.authmore.authorization;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.UserDetails;
import com.ascrud.authmore.oauth.OAuthException;

/**
 * @author walkman
 * @since 2019-02-15
 */
public interface AuthenticationManager {

    UserDetails userValidate(String principal, String credential) throws AuthenticationException;

    ClientDetails clientValidate(String clientId, String scope) throws OAuthException;

    ClientDetails clientValidate(String clientId, String redirectUri, String scope) throws AuthorizationException;
}
