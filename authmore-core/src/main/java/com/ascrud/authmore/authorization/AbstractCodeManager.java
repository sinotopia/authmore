package com.ascrud.authmore.authorization;

import com.ascrud.authmore.ClientDetails;
import com.ascrud.authmore.oauth.CodeManager;

import java.util.Set;

/**
 * @author walkman
 * @since 2019-02-18
 */
public abstract class AbstractCodeManager implements CodeManager {

    @Override
    public void saveCodeBinding(ClientDetails client, String code, Set<String> scopes, String redirectUri, String userId) {
        String clientId = client.getClientId();
        CodeBinding codeBinding = new CodeBinding(code, clientId, scopes, redirectUri, userId);
        saveCode(codeBinding);
    }

    public abstract void saveCode(CodeBinding codeBinding);
}
