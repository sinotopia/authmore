package com.ascrud.authmore;

/**
 * @author walkman
 * @since 2019-02-05
 */
public interface PasswordHolder {

    String getPassword();

    void setPassword(String encoded);
}
