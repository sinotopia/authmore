package com.ascrud.authmore.repositories;

import com.ascrud.authmore.oauth.AccessTokenBinding;
import org.springframework.data.repository.CrudRepository;

/**
 * @author walkman
 * @since 2019-02-21
 */
public interface AccessTokenRepository extends CrudRepository<AccessTokenBinding, String> {}
