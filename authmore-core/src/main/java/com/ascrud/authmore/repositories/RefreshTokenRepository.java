package com.ascrud.authmore.repositories;

import com.ascrud.authmore.oauth.RefreshTokenBinding;
import org.springframework.data.repository.CrudRepository;

/**
 * @author walkman
 * @since 2019-02-26
 */
public interface RefreshTokenRepository extends CrudRepository<RefreshTokenBinding, String> {}
