package com.ascrud.authmore.repositories;

import com.ascrud.authmore.ClientDetails;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * @author walkman
 * @since 2019-02-15
 */
public interface ClientDetailsRepository extends MongoRepository<ClientDetails, String> {

    Optional<ClientDetails> findByClientId(String clientId);
}
