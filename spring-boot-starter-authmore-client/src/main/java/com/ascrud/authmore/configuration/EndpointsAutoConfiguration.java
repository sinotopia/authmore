package com.ascrud.authmore.configuration;

import com.ascrud.authmore.client.ImplicitAuthorizationEndpoint;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author walkman
 * @since 2019-03-07
 */
@Configuration
@ConditionalOnClass({ImplicitAuthorizationEndpoint.class})
@ComponentScan(basePackageClasses = ImplicitAuthorizationEndpoint.class)
public class EndpointsAutoConfiguration {}
