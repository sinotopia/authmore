package com.ascrud.authmore.configuration;

import com.ascrud.authmore.client.AuthorizationTemplate;
import com.ascrud.authmore.client.ClientConfigurationProperties;
import com.ascrud.authmore.client.ClientRestTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;

/**
 * @author walkman
 * @since 2019-03-07
 */
@Configuration
@ConditionalOnClass({ClientRestTemplate.class})
@EnableConfigurationProperties({ClientConfigurationProperties.class})
public class AuthorizeAutoConfiguration {

    private final ClientConfigurationProperties clientProperties;

    public AuthorizeAutoConfiguration(ClientConfigurationProperties clientProperties) {
        this.clientProperties = clientProperties;
    }

    @Bean
    @ConditionalOnMissingBean({AuthorizationTemplate.class})
    public AuthorizationTemplate authorizationTemplate() {
        return new AuthorizationTemplate(clientProperties);
    }

    @Bean
    public AuthenticationManager authenticationManager() {
        return authentication -> null;
    }
}
