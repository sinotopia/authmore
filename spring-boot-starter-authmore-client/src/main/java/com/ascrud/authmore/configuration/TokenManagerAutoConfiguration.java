package com.ascrud.authmore.configuration;

import com.ascrud.authmore.client.*;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import static org.springframework.util.StringUtils.isEmpty;

/**
 * @author walkman
 * @since 2019-03-01
 */
@Configuration
@ConditionalOnClass({ClientRestTemplate.class})
@EnableConfigurationProperties({ClientConfigurationProperties.class})
public class TokenManagerAutoConfiguration implements SmartInitializingSingleton {

    private final String clientId;
    private final String clientSecret;
    private final String tokenIssueUrl;
    private final String redirectUri;

    public TokenManagerAutoConfiguration(
            ClientConfigurationProperties clientConfigurationProperties) {
        this.clientId = clientConfigurationProperties.getClientId();
        this.clientSecret = clientConfigurationProperties.getClientSecret();
        this.tokenIssueUrl = clientConfigurationProperties.getTokenIssueUrl();
        this.redirectUri = clientConfigurationProperties.getRedirectUri();
    }

    @Bean
    @ConditionalOnMissingBean({AuthorizationCodeTokenManager.class})
    public AuthorizationCodeTokenManager clientAuthorizationCodeTokenManager() {
        return new AuthorizationCodeTokenManager(
                createTokenRestTemplate(), clientId, clientSecret, tokenIssueUrl, redirectUri);
    }

    @Bean
    @ConditionalOnMissingBean({PasswordTokenManager.class})
    public PasswordTokenManager clientPasswordTokenManager() {
        return new PasswordTokenManager(createTokenRestTemplate(), clientId, clientSecret, tokenIssueUrl);
    }

    @Bean
    @ConditionalOnMissingBean({ClientCredentialsTokenManager.class})
    public ClientCredentialsTokenManager clientClientCredentialsTokenManager() {
        return new ClientCredentialsTokenManager(
                createTokenRestTemplate(), clientId, clientSecret, tokenIssueUrl);
    }

    @Bean
    @ConditionalOnMissingBean({RefreshTokenManager.class})
    public RefreshTokenManager clientRefreshTokenManager() {
        return new RefreshTokenManager(createTokenRestTemplate(), clientId, clientSecret, tokenIssueUrl);
    }

    private RestTemplate createTokenRestTemplate() {
        return new ClientTokenRestTemplate();
    }

    @Override
    public void afterSingletonsInstantiated() {
        if (isEmpty(clientId) || isEmpty(clientSecret) || isEmpty(tokenIssueUrl))
            throw new IllegalStateException("oauth client must specify a client-id, client-secret and token issue url");
    }
}
