package com.ascrud.authmore.configuration;

import com.ascrud.authmore.client.ClientConfigurationProperties;
import com.ascrud.authmore.client.ClientCredentialsTokenManager;
import com.ascrud.authmore.client.ClientRestTemplate;
import com.ascrud.authmore.common.Assert;
import com.ascrud.authmore.oauth.TokenResponse;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Collections;

/**
 * @author walkman
 * @since 2019-03-04
 */
@Configuration
@Import({TokenManagerAutoConfiguration.class})
@EnableConfigurationProperties({ClientConfigurationProperties.class})
public class RestTemplateAutoConfiguration {

    private final boolean isRequestTokenOnStartup;
    private final String scope;
    private final ClientCredentialsTokenManager tokenManager;

    public RestTemplateAutoConfiguration(
            ClientConfigurationProperties clientConfigurationProperties,
            ClientCredentialsTokenManager tokenManager) {
        this.isRequestTokenOnStartup = clientConfigurationProperties.isRequestTokenOnStartup();
        this.scope = clientConfigurationProperties.getScope();
        this.tokenManager = tokenManager;
    }

    @Bean
    @ConditionalOnMissingBean({ClientRestTemplate.class})
    public ClientRestTemplate clientRestTemplate() {
        if (isRequestTokenOnStartup) {
            Assert.notEmpty(scope, "at least 1 scope is required when requesting token on startup");
            TokenResponse token = tokenManager.getToken(scope, Collections.emptyMap());
            String accessToken = token.getAccess_token();
            return new ClientRestTemplate(accessToken);
        }
        return new ClientRestTemplate();
    }
}
