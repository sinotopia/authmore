package com.ascrud.authmore.admin;

import com.ascrud.authmore.common.BasicController;
import com.ascrud.authmore.common.RandomSecret;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author walkman
 * @since 2019-02-11
 */
@RestController
@RequestMapping("/password")
public class PasswordEndpoint extends BasicController {

    @GetMapping("/random")
    public Map randomPassword() {
        return map().put("result", RandomSecret.create()).map();
    }
}

