package com.ascrud.authmore.admin.client;

import com.ascrud.authmore.ClientDetails;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author walkman
 * @since 2019-01-28
 */
@Repository
public interface ClientDetailsRepo extends MongoRepository<ClientDetails, String> {

    Optional<ClientDetails> findByClientId(String clientId);

    Collection<ClientDetails> findByClientName(String clientName);

    List<ClientDetails> findAllByOrderByClientIdDesc();

    void deleteByClientIdIn(List<String> id);
}
