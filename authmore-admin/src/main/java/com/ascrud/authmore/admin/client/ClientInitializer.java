package com.ascrud.authmore.admin.client;

import com.ascrud.authmore.ClientDetails;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.stereotype.Component;

/**
 * @author walkman
 * @since 2019-01-28
 */
@Component
public class ClientInitializer implements SmartInitializingSingleton {

    private static final String ROOT_APP_ID = "5cb0dd412dc963313f1a90b1";

    private ClientDetailsRepo clientDetailsRepo;

    public ClientInitializer(ClientDetailsRepo clientDetailsRepo) {
        this.clientDetailsRepo = clientDetailsRepo;
    }

    @Override
    public void afterSingletonsInstantiated() {
        ClientDetails client = new ClientDetails(ROOT_APP_ID,
            "authorization_code,password,implicit,client_credentials,refresh_token", "PROFILE",
            "{pbkdf2}30d47c8ef17066e65750bb6469b951dbaf8b40d4cf4b421490ffff92da00804700c8b8fb92cc9ce0",
            "", 999999,
            "SAMPLE", "SA");
        client.setClientName("平台管理");
        clientDetailsRepo.save(client);
    }
}
