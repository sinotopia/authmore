package com.ascrud.authmore.admin.user;

import com.ascrud.authmore.UserDetails;
import com.ascrud.authmore.common.RandomSecret;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author walkman
 * @since 2019-01-28
 */
@Component
public class UserInitializer implements SmartInitializingSingleton {

    private UserDetailsRepo users;
    private PasswordEncoder passwordEncoder;

    public UserInitializer(UserDetailsRepo users, PasswordEncoder passwordEncoder) {
        this.users = users;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void afterSingletonsInstantiated() {
        String randomPassword = RandomSecret.create();
        String rootUsername = "root";
        UserDetails user = users.findByUsername(rootUsername).orElse(
            new UserDetails("root", "SA"))
            .setUserPassword(passwordEncoder.encode(randomPassword));
        try {
            users.save(user);
        } catch (DuplicateKeyException ignored) {
        }

        System.out.println("\n\nAuthmore root password: " + randomPassword + "\n\n");
    }
}
