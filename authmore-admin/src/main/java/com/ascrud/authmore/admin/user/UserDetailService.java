package com.ascrud.authmore.admin.user;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @author walkman
 * @since 2019-01-28
 */
@Component
public class UserDetailService implements UserDetailsService {

    private UserDetailsRepo users;

    public UserDetailService(UserDetailsRepo users) {
        this.users = users;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return users.findByUsername(s).orElse(null);
    }
}
